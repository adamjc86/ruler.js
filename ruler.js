var c;
var monitorSize;
var vRes;
var wRes;
var dpi;
var screenStart = 470;
var currentDistance;
var reqAnimFrame;
var cancelAnimFrame;
var measurement;

function animate()
{	    			
	reqAnimFrame = 
		window.mozRequestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		window.oRequestAnimationFrame;	    
	
	if (currentDistance < dpi)
	{
		reqAnimFrame(animate);
		currentDistance = draw(currentDistance);
	}
}

function draw(currentDistance)
{
	clrscrn();
	var ctx=c.getContext("2d");
	ctx.moveTo(start, 10);
	ctx.lineTo(start, 20);
	ctx.moveTo(start, 15);
	ctx.lineTo(start + currentDistance, 15);
	ctx.moveTo(start + currentDistance, 10);
	ctx.lineTo(start + currentDistance, 20);
	ctx.stroke();
	
	ctx.font = "bold 12px sans-serif";
	ctx.fillText(measurement.toString() + "\"", start + currentDistance - 10, 10);
	
	return currentDistance += 5;  			
}

function calculate()
{	    		    			    		
	c=document.getElementById("rulerCanvas");
	monitorSize = document.getElementById("monitorSize").value;
	measurement = document.getElementById("measurement").value;
	wRes = screen.width;
	vRes = screen.height;
	var di = Math.sqrt(Math.pow(wRes, 2) + Math.pow(vRes, 2));
	dpi = di / monitorSize;
	dpi *= measurement;
	start = screenStart - (dpi / 2);
	currentDistance = 1;
	
	animate();
}

function clrscrn()
{
	var ctx=c.getContext("2d");
	ctx.clearRect(0, 0, c.width, c.height);
	ctx.beginPath();
}	    		
		    		